local enet = require 'enet'
local json = require 'dkjson'
local netconfig = require 'netconfig'
local channels = netconfig.channels

-- global resources
local HOST
local GAME = {}

local function logf(...)
	io.stdout:write(string.format(...),"\n")
end

function GAME:sync(peer)
	local data = {
		msg = "sync",
		randomNumber = GAME.randomNumber,
	}
	if peer then
		-- send to one player
		peer:send(json.encode(data), channels.RELIABLE, "reliable")
	else
		-- send to all players
		HOST:broadcast(json.encode(data), channels.RELIABLE, "reliable")
	end
end

local function onPlayerJoined(peer)
	GAME:sync(peer)
end

local function onPlayerLeft(peer)
end

local function onPlayerMessage(peer, data, _channel)
	GAME.randomNumber = GAME.randomNumber + 1
	GAME:sync()
end

local function onLoad()
	GAME.randomNumber = 0
end

local function onUpdate(dt)
	GAME.now = GAME.now + dt
	if GAME.lastRandomNumberTime == nil or GAME.now > GAME.lastRandomNumberTime + 10 then
		GAME.lastRandomNumberTime = GAME.now
		GAME.randomNumber = math.random(0, 100)
		GAME:sync()
	end
end

local function run()
	-- If you would like to expose your server to the wider internet, use
	-- 0.0.0.0. If you'd only like to listen on your local machine, use
	-- 127.0.0.1. In addition, with most home internet setups, you will need to
	-- configure your router to forward the port you are listening on to the
	-- machine hosting the server.
	local address = string.format("0.0.0.0:%d", netconfig.port)
	logf("listening on %s", address)
	HOST = assert(enet.host_create(address, netconfig.max_connections, netconfig.max_channels))

	onLoad()

	-- game loop. most of the time, our games will be running in the love.run()
	-- game loop. This is written much like that, but only for networking. This
	-- way we can run our server on a thread indefinitely and have it behave
	-- like a game that updates in real time.
	local tick_size = 1/60
	local last_tick = love.timer.getTime()
	while true do
		-- unlike the enet polling on the client, we include a small timeout
		-- here. this is because this loop is the _only_ thing that will be
		-- running on this background thread and we want to yield back to the
		-- OS sometimes, otherwise we'll have a 100% CPU loop hogging the
		-- computer.
		local event = HOST:service(.01)
		if event then
			if event.type == "receive" then
				onPlayerMessage(event.peer, event.data, event.channel)
			elseif event.type == "connect" then
				logf("player joined: %s", event.peer)
				onPlayerJoined(event.peer)
			elseif event.type == "disconnect" then
				logf("player disconnected: %s", event.peer)
				onPlayerLeft(event.peer)
			end
		end

		-- here, we're updating our server simulation about as fast as a 60fps
		-- game. depending on the complexity of your game you can go higher or
		-- lower, and many AAA games do! in the FPS shooter space this is often
		-- called "tickrate".
		local current_time = love.timer.getTime()
		local dt = current_time - last_tick
		if dt >= tick_size then
			onUpdate(dt)
			last_tick = current_time
		end
	end
end
return run
