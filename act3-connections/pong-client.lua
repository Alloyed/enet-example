-- It's pong! the majority of the actual pong logic exists on the server, and
-- this mostly passes inputs on to the server and displays the server state as
-- is.
local enet = require 'enet'
local json = require 'dkjson'
local netconfig = require 'netconfig'

-- global resources
local HOST = enet.host_create()
local CONNECTION
local GAME = {}

local function logf(...)
	io.stdout:write(string.format(...),"\n")
end

local message_handlers = {}

function message_handlers.connected(msg)
	GAME.randomNumber = msg.randomNumber
end

function love.update(dt)
	-- This chunk of code polls for enet events and handles them, once per
	-- frame. the server has an equivalent loop.
	local event = HOST:service()
	while event do
		if event.type == "receive" then
			-- enet can only send and receive strings. to store data
			-- structures, we're using json. Other, more efficient methods can
			-- be used and I encourage you to experiment!
			local data = json.decode(event.data)

			-- each message from the server should have a msg field defined.
			-- this is how we can send multiple different kinds of messages and
			-- have the client respond differently to each.
			if data.msg and message_handlers[data.msg] then
				message_handlers[data.msg](data)
			end
		elseif event.type == "connect" then
			logf("connected to %s.", event.peer)
		elseif event.type == "disconnect" then
			logf("disconnected from %s.", event.peer)
		end
		event = HOST:service()
	end
end

function love.draw()
	love.graphics.print(CONNECTION:state(), 10, 30)
	if GAME.randomNumber then
		love.graphics.print("My random number: " .. GAME.randomNumber, 300, 300)
	end
end

local function onLoad()
	-- watch out: DNS resolution _will_ block the main thread if you don't use
	-- an IP address here! if you'd like to support normal host names I would
	-- recommend either performing your connection during a static loading
	-- screen so a hitch isn't noticable, or use luasocket to perform the DNS
	-- resolution on another thread before passing it to this function
	local address = string.format("127.0.0.1:%s", netconfig.port)
	logf("Connecting to: %s", address)
	CONNECTION = HOST:connect(address, netconfig.max_channels)
end
return onLoad
