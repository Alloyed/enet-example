online pong
===========

hit balls, around the globe.








This is _actually_ intended to be a minimal example of using love and enet to
make a real networked game. It does literally work, in that you can play a game
of pong across two computers, but many of the more complex techniques to make
high-ping play an enjoyable experience are not yet implemented. maybe for a
future tutorial series, maybe~. It can also be used as a reference for
starting to introduce enet-based networking into your own game, and what it
would take to do that.


[Read the tutorial here.](tutorial.md)

[You can also skip to the completed example, if you'd just like reference code.](act4-pong).
