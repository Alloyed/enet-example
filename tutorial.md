How to make videogames but with the power of the internet
=====
Hello online pal, and welcome "how to make videogames but with the power of the internet"!

That's super broad right? I know, that's intentional. I'm going to be building
a path to a specific game (pong) using a specific framework (love2d), and it
will specifically look like this:

[[gif of pong]]

But as much as possible, I want to be focusing on techniques, strategies, and
technical facts that are broadly applicable and can be applied to any
netcode-shaped thing. I am going to be assuming previous programming experience
in love, but this is a netcode tutorial! I will try to avoid assuming you've
written or interacted with network applications before. I'll also try to
include external resources and footnotes where I can find them, so you don't
have to wallow in my opinions and very brief explanations.


If anything is confusing to you or you have a question you don't think the
tutorial answers, you're welcome to [open an issue on
gitlab](https://gitlab.com/Alloyed/enet-example/-/issues), or ask your question
in the [love discord](https://discord.gg/hHDfpP4a2w) if it's more general.


# Act 1

So step one on our journey is to classify the thing we're making in the constellation of online things.

* First: Pong is real time. By that I mean, we want player 1 to move their paddle, and for player 2 to see that happen in such a way that makes it possible for them to play "together".
* Second: Our version of pong will have two players. Some games have more players; but to keep things simple 2 seems good.
* Third: Pong is competitive. I won't talk too long about this in this tutorial, but keep this in the back of your mind: will the decisions we make when implementing this game give one player an advantage over the other?

For our game of pong I will be using [enet](https://leafo.net/lua-enet/), which is a networking library built into love.
It's not the only option available in love, we could also choose to use tcp, udp, or even http via [LuaSocket](https://w3.impa.br/~diego/software/luasocket/home.html); but for most games I think enet should be your default choice.

## Why enet?

Enet handles a lot for you. It supports unreliable packet delivery, like udp.
If you wanted reliable, sequence guaranteed packet delivery, like tcp, enet has
that too. If you wanted to mix and match the two, or have multiple independent
streams of reliable data, you can do that from a single socket. And it
implements a few common game things, like ping calculations and automatic
timeouts.

If all that went over your head, don't worry! That's why you should pick enet
now. That way, your introduction to these concepts will be by using them, instead of
having to implement them from scratch yourself.

## Architecture

There is also the question of making our game peer-to-peer vs client-server.

Ok, that last statement is a lie, there is no question: the technology for
reliable peer-to-peer connectivity, for complex technical/socioeconomic reasons,
is waay harder to come across than for client-server. Therefore, whether we like it 
or not, we're stuck with client-server.

This does not necessarily mean that we need to head out and buy a new dedicated
computer to hold our game server; just that everybody will need to connect to a
specific machine, and that machine will need to be on the same network, or
engage in port forwarding schenanigans to be accessible to the public internet.
More on that to come.

# Act 2
Time for the boring part: the scaffolding. running and testing a multiplayer
game presents a unique challenge; for our tests to actually be multiplayer,
we're gonna need 2 versions of the game client, and a game server.

Here's a link for the [scaffolding](act2-scaffolding) I'm going to be using; you can just download
this folder and use it to follow along. We're actually going to be running our
client and server from the same folder, and even from the same love instance if
we can help it. This is convenient because
1. It makes it very easy to share code between the client and the server, and
2. It give the players (and us) the power to host their own server, on a LAN or otherwise. All they need is a copy of the game :)

To run a game client, we can just run the game as is: it will automatically
include the pong-client.lua file, which is where we can add our client code.
That looks like this, when running it from the command line:

```shell
enet-example/act2-scaffolding$ love .
```

To run a game server, we will use one of two flags. `--dedicated-server` will
run a **dedicated server**, meaning this instance of our game will _only_ be in
charge of running the server. `--listen-server` will run the game server, using
the exact same code, but it will do so in the background of a normal game
client. To demonstrate, if we wanted to test using a dedicated server with
two clients we would need to run three copies of love:

```shell
enet-example/act2$ love . --dedicated-server
enet-example/act2$ love .
enet-example/act2$ love .
```

but with the listen server option, we can slim down to two:

```shell
enet-example/act2$ love . --listen-server
enet-example/act2$ love .
```

The way we accomplish this, and you can read more by look at the
scaffolding project directly, is by using ***threads***. To massively
oversimplify, threads are kind of like independent mini-programs that can run
at the same time as other threads. In the listen server configuration, we'll
have one "main" thread that handles the game client, and one "background"
thread that handles the server, and from the perspective of our game you can
think of them as totally different processes that are just running side-by-side.

## Homework

It's important that you can run everything you need to test the game all at
once: the faster you can do that, the faster you can experiment and make
changes and try them out in the actual game.

In the scaffolding project I've included a [tasks.json](act2-scaffolding/.vscode/tasks.json) file. With it, Visual Studio Code users can just hit ctrl-shift-B to get a listen server and a game client running at the same time. Just make sure that use vscode's "open folder" option on the folder right above the `.vscode` folder, so one of the `actX-whatever` folders.

If you'd prefer to use a different dev environment, don't despair! all you need
to do is the find the equivalent kind of configuration in your setup. Another
tool you may find convenient in this situation, especially on mac OS or linux,
is [tmux](https://www.hamvocke.com/blog/a-quick-and-easy-guide-to-tmux/).

# Act 3

Right, now we can start writing actual networking code. Let's start on the
client side, which should be a bit simpler. First, we'll need a host.
```lua
local enet = require 'enet'
locla netconfig = require 'netconfig'
local HOST = enet.host_create()
```
In enet, the [host](https://leafo.net/lua-enet/#enethost_createbind_address_peer_count_channel_count_in_bandwidth_out_bandwidth) is a top-level manager of your enet connections. Most programs, most of the time, will only ever need one host, even if they need to make multiple connections, or need to shutdown or restart their connections.

Next, we'll need to connect to a server:
```lua
local function onLoad()
	local address = string.format("127.0.0.1:%s", netconfig.port)
	logf("Connecting to: %s", address)
	CONNECTION = HOST:connect(address, netconfig.max_channels)
end
```
This is basically the same as typing in a URL into your favorite browser. by calling [connect](https://leafo.net/lua-enet/#hostconnectaddress__channel_count_data), we're telling enet that we want to start a new connection between our client and the host at the provided address, which can either be an IP address + port (as demonstrated here), or a hostname, like the more common `example.com:8080` format. 

Last (in terms of plumbing), we need to handle enet events as part of our normal update loop:
```lua
local message_handlers = {}
function love.update(dt)
	local event = HOST:service()
	while event do
		if event.type == "receive" then
			local data = json.decode(event.data)

			if data.msg and message_handlers[data.msg] then
				message_handlers[data.msg](data)
			end
		elseif event.type == "connect" then
			logf("connected to %s.", event.peer)
		elseif event.type == "disconnect" then
			logf("disconnected from %s.", event.peer)
		end
		event = HOST:service()
	end
end
```
[events](https://leafo.net/lua-enet/#hostservicetimeout) are one of enet's core concepts, and the main thing you'll be using enet to interact with. Two of the event types, connect and disconnect, are fairly straightforward. They'll tell you when your connection has gone through, or disconnected for any reason. 

## Receiving Data

`recieve` events represent data that we have recieved from the other side of our enet socket. on the client, this means data that the server has sent us, on the server, it means data any one of the clients have sent us. `event.data` holds this data as a string. To make this raw string useful, we're using the [json format](https://www.w3schools.com/js/js_json_intro.asp) to turn raw strings into lua tables. JSON is technically built to describe javascript objects, not lua tables, but they are similar enough that we can just re-use this widely supported standard in lua, too.

Here's what a one of our lua messages might look like with this system:
```lua
myMessage = { msg = "print_text", text = "hello!" }
```
using `json.encode()` on this message should produce a json object that looks pretty similar:
```js
{"msg":"print_text","text":"hello!"}
```
and then on receive side we can turn this back into lua with no loss in information. The next step is to handle the message, and for this we've invented this new `message_handlers` thing. 

## Message Handlers

`message_handlers`, from a theory perspective, can be thought of as a "dispatch mechanism". We know that for our game we want to handle lots of different kinds of messages, and that we want to write special code to handle each kind of message differently. Let's take the "print_text" message as an example: when we recieve an event like that, we probably want to print some text. so here's a potential handler for that situation:
```lua
function message_handlers.print_text(data)
	print(data.text)
end
```
Now let's imagine our existing message run through the system. First, we'll get a data object, and see that it has a "msg" value of `print_text`. Because message_handlers is a simple table, we can look up the `print_text` handler using just that string, and get the function we defined above. Now, when the function is read, it can look for any message-specific values, like our `data.text` field, and do whatever it needs to with it, which in this case we'll just print to stdout.

Then we're done! That's a whole message, and this kind of system works for _any_ message we can dream up, with any kind of fields that we can serialize into json.

## Server

Finally, here's the plumbing for the server: it mirrors client-side plumbing,
except where the client side can piggy-back off of love.update for things that
happen over time, we'll need to invent our own.
```lua
local message_handlers = {}
local function run()
	-- If you would like to expose your server to the wider internet, use
	-- 0.0.0.0. If you'd only like to listen on your local machine, use
	-- 127.0.0.1. In addition, with most home internet setups, you will need to
	-- configure your router to forward the port you are listening on to the
	-- machine hosting the server.
	local address = string.format("0.0.0.0:%d", netconfig.port)
	logf("listening on %s", address)
	HOST = assert(enet.host_create(address, netconfig.max_connections, netconfig.max_channels))

	-- here, we're updating our server simulation about as fast as a 60fps
	-- game. depending on the complexity of your game you can go higher or
	-- lower, and many AAA games do! in the FPS shooter space this is often
	-- called "tickrate".
	local TICK_SIZE = 1/60
	local last_tick = love.timer.getTime()
	while true do
		-- unlike the enet polling on the client, we include a small timeout
		-- here. this is because this loop is the _only_ thing that will be
		-- running on this background thread and we want to yield back to the
		-- OS sometimes, otherwise we'll have a 100% CPU loop hogging the
		-- computer.
		local event = HOST:service(.01)
		if event then
			if event.type == "receive" then
				local data = json.decode(event.data)

				if data.msg and message_handlers[data.msg] then
					message_handlers[data.msg](data)
				end
			elseif event.type == "connect" then
				logf("player joined: %s", event.peer)
			elseif event.type == "disconnect" then
				logf("player disconnected: %s", event.peer)
			end
		end

		local current_time = love.timer.getTime()
		local dt = current_time - last_tick
		if dt >= TICK_SIZE then
			onUpdate(dt)
			last_tick = current_time
		end
	end
end
```
Some of this stuff is subtle, and related to lower-level mechanisms, so here's a higher level way to think about what we built here. A client is a game that updates at 60fps, and responds to messages from the server once-per-frame. A server is a game that updates at TICK_SIZE speed, but will receive (and can respond to) messages from the client as soon as possible.

### Homework

Json is definitely a convenient and effective serialization format, but it has a few downsides for a game. In particular, this format as described uses a lot of bytes to communicate full strings and the json formatting between strings, and the fewer bytes we use, the more data we can send with less latency. Can you imagine/implement a more efficient format?

Also, we haven't covered the specifics in making this game available over a normal internet connection. That's its own research task, and good luck with that :) Just be aware that many bad actors exist online, and so a service that is running 24/7 exposes you to some pretty high security risk, even if you think you're too obscure or unimportant to target.

** Author's note: every section below here is sketched out, but I still need to actually write the tutorial! you can still check out the code/outline to see where this would've gone. **

Act 4
=====
* here's pong implemented on the server -> link to sample project, with game logic but no netcode
* here's how to communicate paddle position from client -> server (unreliable)
* here's how to communicate everything from server -> client (reliable)
* optimization: make paddle/ball position updates special and unreliable
* generic state sync is good! that's how you support drop-in/drop-out
* that's pong babey
* homework: it's a video game! video game it up!

Act 5
=====
* introduce artificial delay creators, maybe demo a windows tool
* show how pong as implemented above has clear host advantage, and results in a worse experience the more ping/packetloss you have
* what can be done about that?
1. client authoritative (this is valid!)
2. buffer everybody's inputs
3. buffer everybody's input, and also lie about the paddle position to make sure it feels responsive
4. buffer inputs, lie, and also interpolate between older states to avoid packetloss stutter
* this is actually all homework! I'm too lazy to do all that in all honesty

