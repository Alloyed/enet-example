-- This is a simple set of shared constants used between the client and server.
return {
    -- Only two people can play pong at once. As a refinement we could also
    -- consider adding a spectator mode, then we could up the max number of connections to include them too.
    max_connections = 2,
    -- All game servers need to listen on a specific port number. This number
    -- doesn't matter, so long as two programs on the same connection don't try
    -- to listen on the same port.
    -- for more, see: https://en.wikipedia.org/wiki/Port_(computer_networking)
    port = 8080,
    -- enet groups messages into "channels". These can be used for whatever you
    -- want in your application, but in general if you use the "reliable" send
    -- mode, messages within a channel are guaranteed to come in order and will
    -- wait for an ealier packet to arrive before giving the application access
    -- to later packets. Different channels will _not_ stall each other in this
    -- way, which can be good if they have different reliability requirements,
    -- but it breaks that guarantee that the order you put things in is the
    -- order that you get them out.
    -- for more info, see: http://enet.bespin.org/Features.html
    max_channels = 2,
    channels = {
        -- Used for all reliable messages. This way we can 100% rely on their
        -- order. Notice that unlike other lua systems, channels start at 0!
        RELIABLE = 0,
        -- used for unreliable position updates. These will be sent out a lot
        -- and it's ok if one goes missing, because we're going to get an update
        -- in a bit anyways with a newer position value.
        POSITION = 1,
    }
}