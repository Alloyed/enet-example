-- Simple example code to run different configurations of the same game:
-- primary goal is to have server code running in an independent thread, and
-- then allow the app to either be dedicated server or shared server-client or
-- just client
love.filesystem.setRequirePath("?.lua;?/init.lua;libs/?.lua")

function love.threaderror(_thread, errorstr)
	error(errorstr)
end

function love.load(args)
	local server, client = false, true
	for _, arg in ipairs(args) do
		if arg == "--dedicated-server" then
			server = true
			client = false
		elseif arg == "--listen-server" then
			server = true
			client = true
		end
	end

	if server then
		local serverThread = love.thread.newThread([[
		io.stdout:setvbuf('line')
		love.filesystem.setRequirePath("?.lua;?/init.lua;libs/?.lua")
		require 'love.filesystem'
		require 'love.timer'
		local run = require 'pong-server'
		run()
		]])
		serverThread:start()
	end
	if client then
		love.window.setMode(800, 600)
		local onLoad = require 'pong-client'
		onLoad()
	end
end
