# ACT 4

This is the completed code for [act 4](../tutorial.md#act-4) of the tutorial. It is also the completed reference, and the best place to copy-paste from if you were planning on doing that.

[download as .zip](https://gitlab.com/Alloyed/enet-example/-/archive/master/enet-example-master.zip?path=act4-pong)

The game itself is split into a only a few files, listed here in the order I think you should read them in:

* [netconfig.lua](netconfig.lua) establishes a few shared config variables, and explains a few high level ideas about enet.
* [pong-client.lua](pong-client.lua) is the game client! It implements almost no logic on its own.
* [pong-server.lua](pong-server.lua) is the game server. It is also where the actual "rules" of pong live.
* [main.lua](main.lua) is pretty small; the only work it does is start the client or the server or both, as appropriate.
