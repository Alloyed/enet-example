-- It's pong! the majority of the actual pong logic exists on the server, and
-- this mostly passes inputs on to the server and displays the server state as
-- is.
local enet = require 'enet'
local json = require 'dkjson'
local netconfig = require 'netconfig'
local channels = netconfig.channels

-- global resources
local HOST = enet.host_create()
local CONNECTION
local GAME

local function logf(...)
	io.stdout:write(string.format(...),"\n")
	io.stdout:flush()
end

local PaddleView = {}
local PaddleView_mt = {__index = PaddleView}

function PaddleView.new(isLocal, id)
	return setmetatable({
		id = id,
		isLocal = isLocal,
		x = 0, y = 0,
		w = 10, h = 140,
	}, PaddleView_mt)
end

function PaddleView:update(_dt)
	if self.isLocal then
		-- this is the local player, we need to update the paddle so they can play the game.
		local _, my = love.mouse.getPosition()
		--
		-- Currently, the local paddle will always be hard snapped to the local
		-- player mouse position to feel responsive, but this strictly speaking
		-- is a lie. all other state is X ms behind right now due to ping, but
		-- this is 0 ms behind. This means that with enough lag you can get
		-- into a situation where it looks like you should've bounced the ball
		-- back but it will go through your paddle. lag hiding/compensation
		-- techniques are complex and varied and will be a follow up tutorial
		-- (i hope~)
		self.y = my

		local data = {y=self.y}
		CONNECTION:send(json.encode(data), channels.POSITION, "unreliable")
	end
end

function PaddleView:getDrawRect()
	local hw, hh = self.w * 0.5, self.h * 0.5
	return self.x - hw, self.y - hh, self.w, self.h
end

function PaddleView:draw()
	love.graphics.rectangle('fill', self:getDrawRect())
end

function PaddleView:deserialize(data)
	-- unpacks data about this paddle from the server and applies it to the local object.
	self.id = data.id
	self.x = data.x
	self.y = data.y
	self.size = data.side
end

local BallView = {}
local BallView_mt = {__index = BallView}

function BallView.new()
	return setmetatable({
		x = 800 / 2, y = 600 / 2,
		radius = 10,
		vx = 0, vy = 0
	}, BallView_mt)
end

function BallView:update(_dt)
	-- does nothing on the client: for now all logic is implemented on the server.
end

function BallView:deserialize(data)
	-- unpacks data about this paddle from the server and applies it to the local object.
	self.x = data.x
	self.y = data.y
end

function BallView:draw()
	love.graphics.circle('fill', self.x, self.y, self.radius)
end

GAME = {}

local message_handlers = {}

-- Each player has an ID associated with them; so we can recognize our own data
-- in sync messages, we'll keep track of the id that is unique to us. We should
-- recieve this message before any sync messages.
function message_handlers.connected(msg)
	GAME.localId = msg.localId
end

-- sync is a catch-all message, useful for updating the entire game at once.
-- this message is written to allow for partial syncs, which is a convenient
-- way to update only the things that have changed.
function message_handlers.sync(msg)
	if msg.paddles then
		for _, paddleData in ipairs(msg.paddles) do
			local isLocal = paddleData.id == GAME.localId
			if not GAME.paddles[paddleData.id] then
				GAME.paddles[paddleData.id] = PaddleView.new(isLocal, paddleData.id)
			end
			if not (msg.skipLocal and isLocal) then
				local paddle = GAME.paddles[paddleData.id]
				paddle:deserialize(paddleData)
			end
		end
	end
	if msg.ball then
		GAME.ball:deserialize(msg.ball)
	end
	if msg.scores then
		GAME.scores = msg.scores
	end
end

-- the ball has been updated! this is a common, spammy message so it's written to be small.
function message_handlers.ball_updated(msg)
	GAME.ball:deserialize(msg)
end

function love.update(dt)
	-- This chunk of code polls for enet events and handles them, once per
	-- frame. the server has an equivalent loop.
	local event = HOST:service()
	while event do
		if event.type == "receive" then
			-- enet can only send and receive strings. to store data
			-- structures, we're using json. Other, more efficient methods can
			-- be used and I encourage you to experiment!
			local data = json.decode(event.data)

			-- each message from the server should have a msg field defined.
			-- this is how we can send multiple different kinds of messages and
			-- have the client respond differently to each.
			if data.msg and message_handlers[data.msg] then
				message_handlers[data.msg](data)
			end
		elseif event.type == "connect" then
			logf("connected to %s.", event.peer)
		elseif event.type == "disconnect" then
			logf("disconnected from %s.", event.peer)
		end
		event = HOST:service()
	end

	-- Game logic. We only want to run this after we know the game has started.
	if CONNECTION:state() == "connected" then
		for _, paddle in pairs(GAME.paddles) do
			paddle:update(dt)
		end
	end
end

function love.draw()
	love.graphics.print(CONNECTION:state(), 10, 30)

	-- Game display. We only want to run this after we know the game has started.
	if CONNECTION:state() == "connected" then
		for _, paddle in pairs(GAME.paddles) do
			paddle:draw()
		end
		GAME.ball:draw()
		love.graphics.printf(GAME.scores.left,  400-305, 30, 300, 'right')
		love.graphics.printf(GAME.scores.right, 405, 30, 300, 'left')
	end
end

local function onLoad()
	-- watch out: DNS resolution _will_ block the main thread if you don't use
	-- an IP address here! if you'd like to support normal host names I would
	-- recommend either performing your connection during a static loading
	-- screen so a hitch isn't noticable, or use luasocket to perform the DNS
	-- resolution on another thread before passing it to this function
	local address = string.format("127.0.0.1:%s", netconfig.port)
	logf("Connecting to: %s", address)
	CONNECTION = HOST:connect(address, netconfig.max_channels)

	GAME.paddles = {}
	GAME.ball = BallView.new()
	GAME.scores = { left = 0, right = 0 }
end
return onLoad
