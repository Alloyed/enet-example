local enet = require 'enet'
local json = require 'dkjson'
local netconfig = require 'netconfig'
local channels = netconfig.channels
local bump = require 'bump'

-- global resources
local HOST
local GAME = {}

local function logf(...)
	io.stdout:write(string.format(...),"\n")
	io.stdout:flush()
end

local Paddle = {}
local Paddle_mt = {__index = Paddle}

function Paddle.new(id, side)
	local self = setmetatable({
		id = id,
		side = side,
		x = 0, y = 0,
		w = 10, h = 140,
		avgV = 0,
		lastY = 0,
		isPaddle = true
	}, Paddle_mt)
	GAME.world:add(self, self:getAABB())
	self:reset()
	return self
end

-- helpers to convert from a convenient center-based coordinate system to
-- bump.lua's top-left based coordinate system
function Paddle:getAABB()
	local hw, hh = self.w * 0.5, self.h * 0.5
	return self.x - hw, self.y - hh, self.w, self.h
end

function Paddle:getTopLeft()
	local hw, hh = self.w * 0.5, self.h * 0.5
	return self.x - hw, self.y - hh
end

function Paddle:setFromTopLeft(x, y)
	local hw, hh = self.w * 0.5, self.h * 0.5
	self.x = x + hw
	self.y = y + hh
end

local function lerp(a, b, t)
	return a + (b - a) * t
end

function Paddle:update(dt)
	local dy = self.y - self.lastY
	dy = dy / dt
	self.avgV = lerp(self.avgV, dy, .5)

	self.lastY = self.y
end

function Paddle:reset()
	if self.side == 'left' then
		self.x = 30
	else
		self.x = GAME.width - 30
	end
	GAME.world:update(self, self:getAABB())
	return self
end

function Paddle:serialize()
	-- packs data about this paddle into a representation we can use to rebuild
	-- this paddle on the client. there is an equivalent :deserialize() method
	-- built into the client padddle object.
	return { id = self.id, x = self.x, y = self.y, side = self.side }
end

function Paddle:deserialize(input)
	-- unpack the players input into their paddle. this is actually pretty
	-- simple, it's just where player's mouse is pointing on the Y-axis, which
	-- we can use to directly set the center of the paddle
	self.y = input.y
	-- In pong, the paddle should always match where the player's cursor is, so
	-- we will teleport the physics object to that exact location.
	GAME.world:update(self, self:getAABB())
end

local Ball = {}
local Ball_mt = {__index = Ball}

function Ball.new()
	local self = setmetatable({
		x = 0, y = 0,
		radius = 10,
		bounceCooldown = 0,
	}, Ball_mt)
	GAME.world:add(self, self:getAABB())
	self:reset()
	return self
end

function Ball:reset()
	-- start the ball at a reasonable speed in one direction of the court.
	self.x = GAME.width / 2
	self.y = GAME.height / 2
	self.vx = math.random() > .5 and 330 or -330 -- pick a direction at random
	self.vy = 10
	GAME.world:update(self, self:getAABB())
	return self
end

-- helpers to convert from a convenient center-based coordinate system to
-- bump.lua's top-left based coordinate system
function Ball:getAABB()
	return self.x-self.radius, self.y-self.radius, self.radius*2, self.radius*2
end

function Ball:getTopLeft()
	return self.x - self.radius, self.y - self.radius
end

function Ball:setFromTopLeft(x, y)
	self.x = x + self.radius
	self.y = y + self.radius
end

local function alwaysBounce(_item, _other)
	return "bounce"
end

function Ball:update(dt)
	-- Ball updating is where the majority of the game "logic" actually lives.
	-- This is 100% simulated on the server.
	local goalX, goalY = self:getTopLeft()
	goalX = goalX + self.vx * dt
	goalY = goalY + self.vy * dt
	local actualX, actualY, collisions = GAME.world:move(self, goalX, goalY, alwaysBounce)
	self:setFromTopLeft(actualX, actualY)

	for _, collision in ipairs(collisions) do
		if collision.other.reflectY then
			self.vy = -self.vy
		elseif collision.other.winSide then
			GAME.onWin(collision.other.winSide)
			-- don't process the rest of the collisions
			return
		elseif collision.other.isPaddle then
			local paddle = collision.other
			-- speed up ball to make the game harder over time
			-- capped to keep the game sane
			self.vx = -math.max(math.min(700, self.vx * 1.1), -700)
			-- add spin to include an element of skill: if you move the paddle to
			-- hit the ball at the last minute the end result is less predictable
			self.vy = lerp(self.vy, paddle.avgV, .5)
		end
	end
end

function Ball:serialize()
	-- packs data about this ball into a representation we can use to rebuild
	-- it on the client. there is an equivalent :deserialize() method built
	-- into the client ball object.
	return { x = self.x, y = self.y }
end

function GAME.onWin(winnerSide)
	-- Game won, time to reset the ball and send that out
	GAME.scores[winnerSide] = GAME.scores[winnerSide] + 1
	GAME.ball:reset()
	local data = {
		msg = "sync",
		ball = GAME.ball:serialize(),
		scores = GAME.scores
	}
	HOST:broadcast(json.encode(data), channels.RELIABLE, "reliable")
end

-- keep track of unique ids: if somebody disconnects and somebody else rejoins,
-- they could have the same index, despite being different connections. We'll
-- keep track of this id when we actually want a key that's globally unique
local nextPlayerId = 0
local function onPlayerJoined(peer)
	local id = nextPlayerId
	nextPlayerId = id + 1
	local data = {
		msg = "connected",
		localId = id,
	}
	peer:send(json.encode(data), channels.RELIABLE, "reliable")

	-- create a paddle/player object for this new connection. we will always
	-- assign the left paddle before assigning the right paddle
	if not GAME.paddles.right then
		GAME.paddles.right = Paddle.new(id, 'right')
		GAME.players[peer:index()] = {side = 'right', id = id}
	elseif not GAME.paddles.left then
		GAME.paddles.left = Paddle.new(id, 'left')
		GAME.players[peer:index()] = {side = 'left', id = id}
	end

	local paddles = {}
	for _, paddle in pairs(GAME.paddles) do
		table.insert(paddles, paddle:serialize())
	end
	data = {
		msg = "sync",
		paddles = paddles,
		ball = GAME.ball:serialize(),
		scores = GAME.scores
	}
	HOST:broadcast(json.encode(data), channels.RELIABLE, "reliable")
end

local function onPlayerLeft(peer)
	local side = GAME.players[peer:index()].side
	GAME.players[peer:index()] = nil
	GAME.paddles[side] = nil

	local paddles = {}
	for _, paddle in pairs(GAME.paddles) do
		table.insert(paddles, paddle:serialize())
	end
	local data = {
		msg = "sync",
		paddles = paddles,
		ball = GAME.ball:serialize(),
		scores = GAME.scores
	}
	peer:send(json.encode(data), channels.RELIABLE, "reliable")
end

local function onPlayerMessage(peer, data, _channel)
	local side = GAME.players[peer:index()].side

	if GAME.paddles[side] then
		local paddle = GAME.paddles[side]
		local input = json.decode(data)
		paddle:deserialize(input)
	end
end

local function onLoad()
	GAME.width = 800 -- the game area width, in pixels
	GAME.height = 600 -- the game area height, in pixels
	GAME.world = bump.newWorld()
	GAME.players = {} -- holds player objects.
	GAME.paddles = { left = nil, right = nil } -- holds paddle objects
	GAME.ball = Ball.new()
	GAME.now = 0 -- the current time in the game simulation, in seconds.
	GAME.scores = { left = 0, right = 0 }

	GAME.world:add({winSide='right'}, -10, 0, 10, GAME.height) -- winning zone for right paddle, on left side
	GAME.world:add({winSide='left'}, GAME.width, 0, 10, GAME.height) -- winning zone for left paddle, on right side
	GAME.world:add({reflectY=true}, 0, -10, GAME.width, 10) -- top wall
	GAME.world:add({reflectY=true}, 0, GAME.height, GAME.width, 10) -- bottom wall
end

local function count(tbl)
	local i = 0
	for _, _ in pairs(tbl) do
		i = i + 1
	end
	return i
end

local function onUpdate(dt)
	GAME.now = GAME.now + dt
	-- only start the game when there are two players
	if count(GAME.players) == 2 then
		GAME.ball:update(dt)
		for _, paddle in pairs(GAME.paddles) do
			paddle:update(dt)
		end

		local paddles = {}
		for _, paddle in pairs(GAME.paddles) do
			table.insert(paddles, paddle:serialize())
		end
		local data = {
			msg = "sync",
			skipLocal = true,
			paddles = paddles,
			ball = GAME.ball:serialize()
		}
		HOST:broadcast(json.encode(data), channels.POSITION, "unreliable")
	end
end

local function run()
	-- If you would like to expose your server to the wider internet, use
	-- 0.0.0.0. If you'd only like to listen on your local machine, use
	-- 127.0.0.1. In addition, with most home internet setups, you will need to
	-- configure your router to forward the port you are listening on to the
	-- machine hosting the server.
	local address = string.format("0.0.0.0:%d", netconfig.port)
	logf("listening on %s", address)
	HOST = assert(enet.host_create(address, netconfig.max_connections, netconfig.max_channels))

	onLoad()

	-- game loop. most of the time, our games will be running in the love.run()
	-- game loop. This is written much like that, but only for networking. This
	-- way we can run our server on a thread indefinitely and have it behave
	-- like a game that updates in real time.
	local tick_size = 1/60
	local last_tick = love.timer.getTime()
	while true do
		-- unlike the enet polling on the client, we include a small timeout
		-- here. this is because this loop is the _only_ thing that will be
		-- running on this background thread and we want to yield back to the
		-- OS sometimes, otherwise we'll have a 100% CPU loop hogging the
		-- computer.
		local event = HOST:service(.01)
		if event then
			if event.type == "receive" then
				onPlayerMessage(event.peer, event.data, event.channel)
			elseif event.type == "connect" then
				logf("player joined: %s", event.peer)
				onPlayerJoined(event.peer)
			elseif event.type == "disconnect" then
				logf("player disconnected: %s", event.peer)
				onPlayerLeft(event.peer)
			end
		end

		-- here, we're updating our server simulation about as fast as a 60fps
		-- game. depending on the complexity of your game you can go higher or
		-- lower, and many AAA games do! in the FPS shooter space this is often
		-- called "tickrate".
		local current_time = love.timer.getTime()
		local dt = current_time - last_tick
		if dt >= tick_size then
			onUpdate(dt)
			last_tick = current_time
		end
	end
end
return run
