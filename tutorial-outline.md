intro/overview
* what is server/client? p2p? which is enet?
* what is reliable delivery? what is unreliable delivery?
* a plea: please use enet even if you just want reliable delivery
* exception: unless you need encryption, or scale i guess, dw about it now

boring workflow stuff
* effectively debugging multiplayer apps often requires running the multiple clients from the same machine
* if you can run the server and client from the same .love app you simplify your life a lot
* here's a framework for doing that with CLI flags -> link to sample project
* homework: what's the most efficient way for you to start up a server and two clients? work within your setup

connections/serialization
* bluh
* here's the snippet to make a connection on both ends
* here's the snippet for using json to serialize and deserialize, and for splitting messages into message handlers
* test with some super simple state sync, maybe show the same random number on both sides? -> link to sample project
* homework: is json the best choice? (no) Can you find a raspi or a VPS so you can test cross-machine connections?

pong
* here's pong implemented on the server -> link to sample project, with game logic but no netcode
* here's how to communicate paddle position from client -> server (unreliable)
* here's how to communicate everything from server -> client (reliable)
* optimization: make paddle/ball position updates special and unreliable
* generic state sync is good! that's how you support drop-in/drop-out
* that's pong babey
* homework: it's a video game! video game it up!

lag compensation
* introduce artificial delay creators, maybe demo a windows tool
* show how pong as implemented above has clear host advantage, and results in a worse experience the more ping/packetloss you have
* what can be done about that?
1. client authoritative (this is valid!)
2. buffer everybody's inputs
3. buffer everybody's input, and also lie about the paddle position to make sure it feels responsive
4. buffer inputs, lie, and also interpolate between older states to avoid packetloss stutter
* this is actually all homework! I'm too lazy to do all that in all honesty
