function love.conf(t)
	t.window = false -- we'll set the window in main.lua
    t.console = true
    io.stdout:setvbuf("line")
end
