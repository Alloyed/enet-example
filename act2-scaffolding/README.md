# ACT 2
This is the completed code for [act 2](../tutorial.md#act-2) of the tutorial. To start following along at home, you can either download this folder as a zip, or [clone the entire repository](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html) using git. 

[download as .zip](https://gitlab.com/Alloyed/enet-example/-/archive/master/enet-example-master.zip?path=act2-scaffolding)

[Skip to the completed act 3 code.](../act3-connections)